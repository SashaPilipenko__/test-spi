export const FETCH_PRODUCTS_PENDING = 'FETCH_PRODUCTS_PENDING';
export const FETCH_PRODUCTS_SUCCESS = 'FETCH_PRODUCTS_SUCCESS';
export const FETCH_PRODUCTS_ERROR = 'FETCH_PRODUCTS_ERROR';
export const FETCH_DELETE_POST = 'FETCH_DELETE_POST';
export const FETCH_UPDATE_POST = 'FETCH_UPDATE_POST';
export const FETCH_CREATE_POST = 'FETCH_CREATE_POST';

export function fetchProductsPending() {
    return {
        type: FETCH_PRODUCTS_PENDING
    }
}

export function fetchProductsSuccess(products) {
    return {
        type: FETCH_PRODUCTS_SUCCESS,
        payload: products
    }
}

export function fetchProductsError(error) {
    return {
        type: FETCH_PRODUCTS_ERROR,
        error: error
    }
}

export function fetchDeletePost(id){
    return {
        type: FETCH_DELETE_POST,
        delete:id
    }
}

export function fetchUpdatePost(item){
    return{
        type:FETCH_UPDATE_POST,
        payload:item
    }
}

export function fetchCreate(item){
    console.log(item);
    return{
        type:FETCH_CREATE_POST,
        payload:item
    }
}