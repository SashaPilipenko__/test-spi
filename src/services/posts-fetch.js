import {fetchProductsPending,fetchUpdatePost,fetchCreate,fetchProductsSuccess, fetchProductsError,fetchDeletePost} from '../actions';


const URL = 'https://jsonplaceholder.typicode.com/posts';
function fetchPostsAction(){
    return dispatch => {
        fetchProductsPending(true);

        fetch(URL)
        .then(res =>  res.json())
        .then(res =>{
            if(res.error){
                throw(res.error);
            }
            console.log(res)
            dispatch(fetchProductsSuccess(res));
            return res;
        })
        .catch(error => {
            dispatch(fetchProductsError(error));
        })
    }
}

function fetchSinglePostAction(id){
    return dispatch => {
        fetchProductsPending(true);
        const URLITEM = `${URL}/${id}`;
        fetch(URLITEM)
        .then(res => res.json())
        .then(res =>{
            if(res.error){
                throw(res.error);
            }
            console.log(res)
            dispatch(fetchProductsSuccess(res));
            return res;
        })
        .catch(error => {
            dispatch(fetchProductsError(error));
        })
    }
}

function fetchDelete(id){
    return dispatch => {
        console.log(id);
        const URLITEM = `${URL}/${id}`;
        console.log(URLITEM);
        fetch(URLITEM,{
            method: 'DELETE'
          })
        .then(res => {
            dispatch(fetchDeletePost(id))
            return res;
        })
        
        .catch(error => {
            dispatch(fetchProductsError(error));
        })
    }
}

function fetchUpdate(id,title,body){
    return dispatch => {
        console.log(id);
        const URLITEM = `${URL}/${id}`;
        console.log(URLITEM);
        fetch(URLITEM, {
    method: 'PUT',
    body: JSON.stringify({
      id: id,
      title: title,
      body: body,
      userId: 1
    }),
    headers: { 
      "Content-type": "application/json; charset=UTF-8"
    }
  })
  .then(response => response.json())
  .then(json => {
      return dispatch(fetchUpdatePost(json));
  })
    }
}

function fetchCreateActive(title,body){
    return dispatch => {
        console.log(URL);
        fetch(URL, {
    method: 'POST',
    body: JSON.stringify({
      title: title,
      body: body,
      userId: 1
    }),
    headers: {
      "Content-type": "application/json; charset=UTF-8"
    }
  })
  .then(response => response.json())
  .then(json => {dispatch(fetchCreate(json))})
  
    }
}

export  {fetchPostsAction,fetchSinglePostAction,fetchDelete,fetchUpdate,fetchCreateActive};