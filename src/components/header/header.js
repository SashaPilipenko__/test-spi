import React from 'react';
import {
    Link
  } from "react-router-dom";

 const Header = (props) => {
    return(
        <div className=" container header navbar navbar-expand-lg navbar-light bg-light">
        <ul className="navbar-nav mr-auto">
            <li className="nav-item"><Link to="/">Головна</Link></li>
            <li className="nav-item"><Link to="/posts">Новини</Link></li>
            <li className="nav-item"><Link to="/profile">Профіль</Link></li>
        </ul>
        </div>
    );

}

export default Header;

