import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Header from '../header/header';
import Posts from '../posts/posts';
import Profile from '../profile/profile';
import Login from '../login/login';
import SinglePost from '../single-post/single-post';

import './App.css';


class App extends React.Component{

  render(){
      return (
      <Router>
         <Header/>
          <div className="App container">
          
            <Switch>

              <Route path="/" exact={true} render={()=> 
              <div>
                <h1>This is a Main page</h1>
                <SinglePost activeId={2}/>
                </div>}>

              </Route>

              <Route path="/posts" exact={true}>
                <Posts/>
              </Route>

              <Route path="/profile" exact={true}>
                <Profile />
              </Route>

              <Route path="/login" exact={true}>
                <Login />
              </Route>

              <Route path="/posts/:id" exact={true} render={({match})=> <SinglePost activeId={match.params.id}/>}/>
            
            </Switch> 
         </div>
      </Router>
    );
  }
}

export default App;
