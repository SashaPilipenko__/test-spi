import React from 'react';
import {fetchSinglePostAction} from '../../services/posts-fetch';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import Spinner from '../spinner/spinner';

 class SinglePost extends React.Component{
    componentWillMount(){
        this.props.fetchSinglePostAction(this.props.activeId);
    }
    render(){
        const {post,load} = this.props;
        const spin = (load) ? <Spinner/> : null;
        console.log(post);
        return(
            
            <div className="jumbotron jumbotron-fluid">
                {spin}
                <div className="container">
                    <h1 className="display-4">{post.title}</h1>
                    <p className="lead">{post.body}</p>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
      post : state.singlePost,
      load: state.pending
    }
  }
  
  const mapDispatchToProps = (dispatch) => {
    const actions = bindActionCreators({fetchSinglePostAction},dispatch);
    return {...actions};
  };

  export default connect(mapStateToProps,mapDispatchToProps)(SinglePost);