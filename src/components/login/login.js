import React from 'react';
import {Redirect} from 'react-router-dom';

export default class Login extends React.Component {
    state = {
        login:'',
        password:''
    }

     Onlogin = (e) => {
         e.preventDefault();
         if(this.state.login === 'sasha' && this.state.password === '123'){
            localStorage.login = 'true';
         }
        this.setState({
            login:'',
            password:'',
            badway:'Incorrect password or email'
        });
    }
    
    onChange1 = (e) => {
        this.setState({
            login: e.target.value
        });
    }

    onChange2 = (e) => {
        this.setState({
            password: e.target.value
        });
    }

    render(){

        if(localStorage.login !== 'true'){
            return(
                <form onSubmit={this.Onlogin}>
                    <input className="form-control" placeholder="Login" type="text" onChange={this.onChange1} value={this.state.login}/>
                    <input className="form-control" placeholder="Password" type="password" onChange={this.onChange2} value={this.state.password} />
                    <button className="btn btn-primary" type="submit">Login</button>
                    {this.state.badway}
                </form>
            );
        }

        console.log('you alredy login!');
            return <Redirect to="/profile" />
    
       
    }
    
}