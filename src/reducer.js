import {FETCH_PRODUCTS_PENDING,FETCH_CREATE_POST,FETCH_UPDATE_POST, FETCH_PRODUCTS_SUCCESS, FETCH_PRODUCTS_ERROR,FETCH_DELETE_POST} from './actions';

const initialState = {
    pending: true,
    posts: [],
    error: null,
    singlePost:{}
}

export const reducer = (state = initialState,action) => {
    switch(action.type) {
        case FETCH_PRODUCTS_PENDING: 
            return {
                ...state,
                pending: true
            }
        case FETCH_PRODUCTS_SUCCESS:
            if(Array.isArray(action.payload)) {
                return {
                    ...state,
                    pending: false,
                    posts: action.payload
                }
            }

            if(typeof(action.payload) == 'object' ) {
                return {
                    ...state,
                    pending: false,
                    singlePost: action.payload
                }
            }
            break;
            
        case FETCH_PRODUCTS_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error
            }

            case FETCH_DELETE_POST :
                const arr = state.posts.slice();
                console.log(action.delete);
                arr.splice(action.delete-1,1);
            return{
                ...state,
                posts: arr
            }
        case FETCH_UPDATE_POST:
            const arr2= state.posts.slice();
           const el = arr2[action.payload.id - 1];
           el.title = action.payload.title;
           el.body = action.payload.body;
            console.log('Запись номер ' + action.payload.id + ' теперь такая ' + action.payload);
            console.log(state);
            return{
                ...state,
                posts:arr2
            }

        case FETCH_CREATE_POST:
            const arr3 = state.posts.slice();
            console.log(action.payload);
            arr3.unshift(action.payload);
            console.log(arr3);
            return{
                ...state,
                posts:arr3
            }
        default: 
            return state;
    }
}



